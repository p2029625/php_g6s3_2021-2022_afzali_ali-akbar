<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" type="text/css" href="../style/style.css"/>
	</head>
<body>
        
        <ul class="menu">  <!-- barre de navigation -->
              <li>
                <a href="../index.php" class="actif">Accueil</a>
              </li>
        </ul>

            <h1 id="bnv"> S'inscrire </h1>
			<form action="../modeles/m_registration.php" method="POST">	<!-- Formulaire d'inscription -->	
				<div>
					<label>Mail</label>
					<input type="text" name="mail" />
				</div>
				<div>
					<label>Nom d'utilisateur</label>
					<input type="text" name="username" />
				</div>
				<div>
					<label>Mot de passe</label>
					<input type="password" name="password" />
				</div>
				<br />
				<div>
					<button name="register" class="bt_log">S'inscrire</button>
				</div>
				<a href="v_login.php">Se connecter</a>
			</form>
</body>
</html>