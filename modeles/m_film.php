<?php

$bdd = connectDb(); //connexion à la BDD
 $queryListColumn = $bdd->prepare("DESCRIBE film");
 $queryListColumn->execute();
 $listColumn = $queryListColumn->fetchAll (PDO::FETCH_COLUMN);

 require_once("controleurs/c_film.php");
 
 $query = $bdd->prepare('SELECT* FROM film ORDER BY '. $orderBy .' '.$orderDirection); // requête SQL
 $query->execute(); // paramètres et exécution
 $data = $query->fetchAll();

 ?>