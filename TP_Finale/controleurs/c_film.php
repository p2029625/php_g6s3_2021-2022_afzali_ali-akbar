<?php

if (isset($_GET["orderBy"])
     && isset($_GET["orderDirection"]) 
     && in_array($_GET["orderBy"],$listColumn) 
     && in_array($_GET["orderDirection"],array('ASC', 'DESC'))) {
     $orderBy = $_GET["orderBy"];
     $orderDirection = $_GET["orderDirection"];
 } else {
     $orderBy = "id";
     $orderDirection = "ASC";
 }

 ?>