<?php 
    include_once("../modeles/m_film.php");
?>
<html> 
     <head>
          <title> Base de données </title>
          <link rel="stylesheet" href="../style/style.css">
     </head>

        
     <body> 
     <ul class="menu">
              <li>
                <a href="../vues/v_film_guest.php" class="actif">Accueil</a>
              </li>
              <li>
                <a href="v_login.php">Connexion</a>
              </li>
        </ul>
        <h1 id='bnv'> Bienvenue sur CinéSite ! </h1>
          <table border="1">
          <caption id="cap" align=bottom>Liste des films enregistrés dans notre base de données</caption>
          <thead>
               <tr>
                    <th>ID
                         <a href="v_film_guest.php?orderBy=id&orderDirection=ASC" id="haut">🔼</a>
                         <a href="v_film_guest.php?orderBy=id&orderDirection=DESC" id="bas">🔽</a>
                    </th>
                    <th>nom
                         <a href="v_film_guest.php?orderBy=nom&orderDirection=ASC" id="haut">🔼</a>
                         <a href="v_film_guest.php?orderBy=nom&orderDirection=DESC" id="bas">🔽</a>
                    </th>
                    <th>annee
                         <a href="v_film_guest.php?orderBy=annee&orderDirection=ASC" id="haut">🔼</a>
                         <a href="v_film_guest.php?orderBy=annee&orderDirection=DESC" id="bas" >🔽</a>
                    </th>
                    <th>score
                         <a href="v_film_guest.php?orderBy=score&orderDirection=ASC" id="haut">🔼</a>
                         <a href="v_film_guest.php?orderBy=score&orderDirection=DESC" id="bas">🔽</a>
                    </th>
                    <th>nbVotant
                         <a href="v_film_guest.php?orderBy=nbVotant&orderDirection=ASC" id="haut">🔼</a>
                         <a href="v_film_guest.php?orderBy=nbVotant&orderDirection=DESC" id="bas" >🔽</a>
                    </th>
               </tr>
          </thead>
          <tbody>
               <?php 
                foreach($data as $key=>$row) // lecture par ligne
                            
               {
               ?>
                    <tr>
                    <th><?php echo $row['id'];?></th>
                    <th><a href="../vues/v_descFilm.php?nom=<?php echo $row['nom'];?>"><?php echo $row['nom'];?></a></th>
                    <th><?php echo $row['annee'];?></th>
                    <th><?php echo $row['score'];?></th>
                    <th><?php echo $row['nbVotants'];?></th>
                    </tr>
               <?php
               } 
               ?>
          </tbody>
              
          </table>

          <class id="tips">
              <p> Pour pourvoir intéragir avec la liste (ajout, suppression, vote) veuillez vous connecter ou créer un nouveau compte ! </p>
            </class>
            
        <img src="../icones/banniere.jpg" id="banniere">

</body>   

<style>
 
</style>
