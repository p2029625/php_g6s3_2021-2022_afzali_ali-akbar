
<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet" type="text/css" href="../style/style.css"/>
</head>
<body>
		<ul class="menu"> <!-- barre de navigation -->
              <li>
                <a href="../index.php" class="actif">Accueil</a>
              </li>
        </ul>
		<h1 id="bnv"> Connexion </h1>	
				
			<form action="../modeles/m_login.php" method="POST">	
				<div>
					<label>Nom d'utilisateur</label>
					<input type="text" class="form-control" name="username" />
				</div>
				<div>
					<label>Mot de pass</label>
					<input type="password" class="form-control" name="password" />
				</div>
				<br />
				<div>
					<button name="login" class="bt_log">Connexion</button>
				</div>
				<a href="v_registration.php">Pas encore de compte? S'inscrire</a>
			</form>
</body>
</html>