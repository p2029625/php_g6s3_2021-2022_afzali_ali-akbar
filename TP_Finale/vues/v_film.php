<?php 
    include_once("../modeles/m_film.php");
    include_once("../fonctions/f_film.php")
?>
<html> 
     <head>
          <title> Base de données </title>
          <link rel="stylesheet" href="../style/style.css">
     </head>

        
     <body> 
     <ul class="menu">
              <li>
                <a href="../vues/v_film.php" class="actif">Accueil</a>
              </li>
              <li>
                   <a href="../vues/v_addFilm.php">Ajouter un film</a>
               </li>
              <li>
                <a href="../modeles/m_logout.php">Déconnexion</a>
              </li>
        </ul>
          <h1 id="bnv"> CinéSité </h1>
          <table border="1">
          <thead>
          <caption id="cap" align=bottom>Liste des films enregistrés dans notre base de données</caption>
               <tr>
                    <th>ID
                         <a href="v_film.php?orderBy=id&orderDirection=ASC" id="haut">🔼</a>
                         <a href=v_film.php?orderBy=id&orderDirection=DESC" id="bas">🔽</a>
                    </th>
                    <th>nom
                         <a href="v_film.php?orderBy=nom&orderDirection=ASC" id="haut">🔼</a>
                         <a href="v_film.php?orderBy=nom&orderDirection=DESC" id="bas">🔽</a>
                    </th>
                    <th>annee
                         <a href="v_film.php?orderBy=annee&orderDirection=ASC" id="haut">🔼</a>
                         <a href="v_film.php?orderBy=annee&orderDirection=DESC" id="bas" >🔽</a>
                    </th>
                    <th>score
                         <a href="v_film.php?orderBy=score&orderDirection=ASC" id="haut">🔼</a>
                         <a href="v_film.php?orderBy=score&orderDirection=DESC" id="bas">🔽</a>
                    </th>
                    <th>nbVotant
                         <a href="v_film.php?orderBy=nbVotant&orderDirection=ASC" id="haut">🔼</a>
                         <a href="v_film.php?orderBy=nbVotant&orderDirection=DESC" id="bas" >🔽</a>
                    </th>
                    <th> Vote </th>
                    <th> Action </th>
               </tr>
          </thead>
          <tbody>
               <?php 
                foreach($data as $key=>$row) // lecture par ligne
                            
               {
               ?>
                    <tr>
                    <th><?php echo $row['id'];?></th>
                    <th><a href="../vues/v_descFilm.php?nom=<?php echo $row['nom'];?>"><?php echo $row['nom'];?></a></th>
                    <th><?php echo $row['annee'];?></th>
                    <th><?php echo $row['score'];?></th>
                    <th><?php echo $row['nbVotants'];?></th>
                    <th><a href="../modeles/m_vote.php?id=<?php echo $row['id'];?>&&action=up"><img src="../icones/poucehaut.png"></a><a href="../modeles/m_vote.php?id=<?php echo $row['id'];?>&&action=down"><img src="../icones/poucebas.png"></a></th>
                    <th><a href="../modeles/m_delete.php?id=<?php echo $row['id'];?>"><img src="../icones/bin.png" id="logo"></a></th>
                    </tr>
               <?php
               } 
               ?>
          </tbody>
              
          </table>

</body>   

