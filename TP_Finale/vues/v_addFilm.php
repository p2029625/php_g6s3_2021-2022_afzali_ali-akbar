<!DOCTYPE 
<html>  
        <head>
            <link rel="stylesheet" type="text/css" href="../style/style.css"/>
            <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1"/>
        </head>
            <ul class="menu">
                <li>
                    <a href="../vues/v_film.php" class="actif">Accueil</a>
                </li>
                <li>
                    <a href="../modeles/m_logout.php">Déconnexion</a>
                </li>
            </ul>
        <h1 id="bnv"> Formulaire d'ajout de film </h1>
        <form action="../modeles/m_addFilm.php" method="post">
            <p>ID du Film : <input type="text" name="id" placeholder="1, 2 ,3..."/></p>
            <p>Nom du Film : <input type="text" name="nom" placeholder="Terminator, Avengers.."/></p>
            <p>Annee du Film : <input type="text" name="annee" placeholder="2015, 2018.." /></p>
            <p>Score du Film : <input type="text" name="score" placeholder="4.5 ..."/></p>
            <p>Nombres de votants du Film :  <input type="text" name="nbVotants" placeholder="400, 578..." /></p>
            <p><input type="submit" name="save" value="Entrez ce film dans la BD"></p>
            </form>
</html>