<?php
	session_start();
	
	require_once '../fonctions/f_film.php';
	
	if(ISSET($_POST['login'])){
		if($_POST['username'] != "" || $_POST['password'] != ""){
			$username = $_POST['username'];
			$password = $_POST['password'];
			$sql = "SELECT * FROM `logs` WHERE `username`=? AND `password`=? ";
            $conn = connectDb();
			$query = $conn->prepare($sql);
			$query->execute(array($username,$password));
			$row = $query->rowCount();
			$fetch = $query->fetch();
			if($row > 0) {
				$_SESSION['user'] = $fetch['id'];
                $p = $fetch['id'];
				header("location: ../vues/v_accueil.php");
			} else{
				echo "
				<script>alert('Nom d'utilisateur ou mot de passe incorrect!')</script>
				<script>window.location = '../vues/v_login.php'</script>
				";
			}
		}else{
			echo "
				<script>alert('Veuillez complétez les champs!')</script>
				<script>window.location = '../vues/v_login.php'</script>
			";
		}
	}
?>